$(document).ready(function() {
    $('#demand').select2({
        theme: "classic",
        placeholder: "Nhập thông tin nhu cầu",
        allowClear: true
    });
    $('#target_land_types').select2({
        theme: "classic",
        placeholder: "Tìm kiếm loại bất động sản",
        allowClear: true
    });
    $('#province_city').select2({
        theme: "classic",
        placeholder: "Tỉnh/ Thành",
        allowClear: true
    });
    $('#district').select2({
        theme: "classic",
        placeholder: "Quận/ Huyện",
        allowClear: true
    });
    $('#ward').select2({
        theme: "classic",
        placeholder: "Phường/ Xã",
        allowClear: true
    });
    $('#street').select2({
        theme: "classic",
        placeholder: "Đường, phố",
        allowClear: true
    });
    $('#target_details').select2({
        theme: "classic",
        placeholder: "Thêm thông tin nhân khẩu học, sở thích, hành vi",
        allowClear: true
    });
    $('#keywords_ai').select2({
        theme: "classic",
        placeholder: "Nhập từ khóa",
        allowClear: true
    });
    $('#solvency').select2({
        theme: "classic",
        placeholder: "Nhập thông tin khả năng thanh toán",
        allowClear: true
    });
    $('#area').select2({
        theme: "classic",
        placeholder: "Nhập thông tin diện tích",
        allowClear: true
    });
    $('#utilities').select2({
        theme: "classic",
        placeholder: "Nhập tiện ích tại đây",
        allowClear: true
    });
    $('#near_utilities').select2({
        theme: "classic",
        placeholder: "Nhập tiện ích tại đây",
        allowClear: true
    });
    $("#feng_shui").select2({
        theme: "classic",
        placeholder: "Nhập tuổi, mệnh",
        allowClear: true
    });
    $(".age-range-input").jRange({
        from: 10,
        to: 70,
        step: 1,
        scale: [10,20,30,40,50,60,70],
        format: '%s',
        width: 570,
        showLabels: true,
        isRange : true,
        theme: "theme-blue"
    });
});